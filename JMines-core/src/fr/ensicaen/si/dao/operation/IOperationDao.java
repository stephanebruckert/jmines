package fr.ensicaen.si.dao.operation;

import java.util.List;

import fr.ensicaen.si.model.Operation;
import fr.ensicaen.si.model.Result;

public interface IOperationDao {
	
	public List<Operation> getById(int id);
	public Result getByName(String lastName);
	public Result getByFullName(String lastName, String firstName);
	
}
