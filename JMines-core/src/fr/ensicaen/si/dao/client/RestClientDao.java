package fr.ensicaen.si.dao.client;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fr.ensicaen.si.model.Client;

public class RestClientDao extends AClientDao {

	public RestClientDao() {
		getFromRest();
	}
	
	private void getFromRest() {
		BufferedReader br = null;
		try {
			URL url = new URL("http://localhost:8080/TJ2013-RESTful_WS/si/clients/");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "*/*");
	 
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}
	 
			br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));
	 
			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
				try {
					doXML(output);
				} catch (XPathExpressionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SAXException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParserConfigurationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			conn.disconnect();
		  } catch (MalformedURLException e) {
			e.printStackTrace();
		  } catch (IOException e) {
			e.printStackTrace();
		  }
	}
	
	@Override
	public int countClient() {
		return clients.size();
	}

	@Override
	public List<Client> getClients() {
		return clients;
	}

	@Override
	public List<Client> getByName(String name) {
		List<Client> requestedClients = new ArrayList<Client>();
		for (Client c : clients) {
			if (c.getLast().startsWith(name)) {
				requestedClients.add(c);
			}
		}
		return requestedClients;
	}

	@Override
	public List<Client> getByFullName(String name, String firstName) {
		List<Client> requestedClients = new ArrayList<Client>();
		for (Client c : clients) {
			if (c.getLast().startsWith(name) && c.getFirst().startsWith(firstName)) {
				requestedClients.add(c);
			}
		}
		return requestedClients;
	}

	@Override
	public Client getById(int id) {
		for (Client c : clients) {
			if (c.getId() == id) {
				return c;
			}
		}
		return null;
	}
	
	private void doXML(final String str) throws XPathExpressionException, SAXException, IOException, ParserConfigurationException {
		Client c;
		final Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes()));
	    final javax.xml.xpath.XPathExpression xPathExpression = XPathFactory.newInstance().newXPath().compile("//client/id/text()");
	    final NodeList nodeList = (NodeList) xPathExpression.evaluate(document, XPathConstants.NODESET);
	    final List<String> values = new ArrayList<String>();
	    for (int i = 0; i < nodeList.getLength(); ++i) {
	        values.add(nodeList.item(i).getNodeValue());
	    }
	    System.out.println(values);
	    
	    final javax.xml.xpath.XPathExpression xPathFirsts = XPathFactory.newInstance().newXPath().compile("//client/first/text()");
	    final NodeList firstsList = (NodeList) xPathFirsts.evaluate(document, XPathConstants.NODESET);
	    final List<String> firsts = new ArrayList<String>();
	    for (int i = 0; i < firstsList.getLength(); ++i) {
	        firsts.add(firstsList.item(i).getNodeValue());
	    }
	    System.out.println(firsts);
	    
	    final javax.xml.xpath.XPathExpression xPathlasts = XPathFactory.newInstance().newXPath().compile("//client/last/text()");
	    final NodeList lastsList = (NodeList) xPathlasts.evaluate(document, XPathConstants.NODESET);
	    final List<String> lasts = new ArrayList<String>();
	    for (int i = 0; i < lastsList.getLength(); ++i) {
	        lasts.add(lastsList.item(i).getNodeValue());
	    }
	    System.out.println(lasts);
	    
	    int id;
	    String first;
	    String last;
	    int i = 0;
	    for (String s : values) {
	    	id = Integer.valueOf(s);
	    	last = lastsList.item(i).getTextContent();
	    	first = firstsList.item(i).getTextContent();
	    	c = new Client(id, last, first);
	    	i++;
	    	clients.add(c);
	    }
	    System.out.println(clients);
	    
	}

}
