package fr.ensicaen.si.web;

import fr.ensicaen.si.dao.client.ClientDao;
import fr.ensicaen.si.dao.client.DbClientDao;
import fr.ensicaen.si.dao.operation.DbOperationDao;
import fr.ensicaen.si.dao.operation.OperationDao;
import fr.ensicaen.si.model.Result;

public class SiBean {

	public Result getOperationsByFirstAndLastName(String last, String first) {
		ClientDao cd = ClientDao.getInstance();
		cd.setDelegate(new DbClientDao());
		OperationDao od = OperationDao.getInstance();
		od.setDelegate(new DbOperationDao());
		return OperationDao.getInstance().getByFullName(last, first);
	}

}
