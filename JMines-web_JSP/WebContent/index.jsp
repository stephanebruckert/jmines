<%@page import="fr.ensicaen.si.model.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="SiBean" class="fr.ensicaen.si.web.SiBean"
	scope="session" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TP Si</title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<%
	String last = "";
	String first = "";
	if (request.getParameter("last") != null) {
		last = request.getParameter("last");
	}
	if (request.getParameter("first") != null) {
		first = request.getParameter("first");
	}
	%>
	<form method="get" action="index.jsp">
		<div id="searchFields">
			<div class="styled_input">
				<label for="idLast">Last</label><input id="idLast"
					name="last" type="text" value="<%=last%>" />
			</div>

			<div class="styled_input">
				First<input id="idFirst" type="text" name="first" 
						value="<%=first%>" />
			</div>
		</div>
		<input type="submit" name="submit" value="Submit">
	</form>
	<div>
	<% 
		Result result = SiBean.getOperationsByFirstAndLastName(last, first);

		for(Client c :result.getClients()) {
			%><a href="index.jsp?first=<%=c.getFirst()%>&last=<%=c.getLast()%>"><%=c.getFirst()%> <%=c.getLast()%></a><br /><%
		}
		if (result.getOperations().size() > 0) {
			%>Operations:<%
			for(Operation o : result.getOperations()) {
				%> <%=o.getId() %><%
			}
		} else {
			%>No operations for this client<%
		}
		
		%><a href="index.jsp">All clients</a><%
	%>
	</div>
</body>
</html>