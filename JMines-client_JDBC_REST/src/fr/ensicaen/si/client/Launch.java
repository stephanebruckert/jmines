package fr.ensicaen.si.client;

import java.awt.EventQueue;

public class Launch {
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChooseDatabase cd = new ChooseDatabase();
					cd.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
