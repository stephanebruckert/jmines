package fr.ensicaen.si.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.rmi.RemoteException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;


/**
 * Displays the welcome view window
 * @author stephane
 */
public class ChooseDatabase extends JFrame {
	
	/** Serial Version UID */
	private static final long serialVersionUID = 1L;

	private JButton dbButton;
	private JButton restButton;
	private JPanel loginPanel;
	
	/**
	 * Constructor
	 */
	public ChooseDatabase(){
		super("Welcome");
		this.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		this.setLocationRelativeTo(null);
		this.setLayout(new BorderLayout());
		
		this.loginPanel = new JPanel();
		this.add(loginPanel);
		this.dbButton = new JButton("DB");
		this.restButton = new JButton("REST");
		this.loginPanel.add(dbButton);
		this.loginPanel.add(restButton);

		//JPanel properties
		this.setVisible(true);
		this.setSize(200, 85);
		actionlogin();
	}
	
	/**
	 * Called when the login button is clicked
	 */
	public void actionlogin(){
		restButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				Frame frame = new Frame(true);
				dispose();
			}
		});
		dbButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				Frame frame = new Frame(false);
				dispose();
			}
		});
	}

}