package fr.ensicaen.si.client.listeners;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;

import fr.ensicaen.si.dao.operation.OperationDao;
import fr.ensicaen.si.model.Client;
import fr.ensicaen.si.model.Operation;
import fr.ensicaen.si.model.Result;

public class AutoCompleteListener implements DocumentListener {
	private JTextField clientLastName;
	private JTextField clientFirstName;
	private JTable clientTable;
	private JTable operationTable;
	
	public AutoCompleteListener(JTextField clientLastName, JTextField clientFirstName,
			JTable clientModel, JTable operationModel) {
		this.clientLastName = clientLastName;
		this.clientFirstName = clientFirstName;
		this.clientTable = clientModel;
		this.operationTable = operationModel;
	}
	
	@Override
	public void insertUpdate(DocumentEvent arg0) {
		this.clientTable.clearSelection();
		warn();
	}
	
	@Override
	public void removeUpdate(DocumentEvent arg0) {
		this.clientTable.clearSelection();
		warn();
	}
	
	public void warn() {
		//Getting actual results
		Result result = OperationDao.getInstance().getByFullName(
				clientFirstName.getText(), clientLastName.getText());
		
		DefaultTableModel clientModel = (DefaultTableModel) clientTable.getModel();
		DefaultTableModel operationModel = (DefaultTableModel) operationTable.getModel();
		
		clientModel.setRowCount(0);
		
		//Fill it again
		for (Client c : result.getClients()) {
			clientModel.addRow(
				new Object[] {
						c.getFirst(),
						c.getLast()
				}
			);
		}
		
		//Empty the table
		operationModel.setRowCount(0);

		//Fill it again
		for (Operation o : result.getOperations()) {
			operationModel.addRow(
				new Object[] {
						o.getCardNum(), 
						o.getAccountNum(),
						o.getAmount(),
						o.getDate()
				}
			);
		}
	}

	@Override
	public void changedUpdate(DocumentEvent arg0) {}
}
