package fr.ensicaen.si.resources;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import fr.ensicaen.si.dao.client.ClientDao;
import fr.ensicaen.si.model.Client;

@Path("/clients")
public class ClientsResources {

	@Context 
	UriInfo uriInfo; 
	@Context 
	Request request;
	
	/** 
	 * Displays all clients
	 */
	@GET
	@Produces(MediaType.TEXT_XML)
	public List<Client> getAllClients() {
		return ClientDao.getInstance().getClients();
	}
	
	/**
	 * Counts all clients
	 * @return
	 */
	@GET
	@Path("count")
	@Produces(MediaType.TEXT_XML)
	public String getCount() {
		return String.valueOf(ClientDao.getInstance().countClient());
	}
	
	/**
	 * Get client from last name
	 * @param last
	 * @return
	 */
	@GET
	@Path("name/{last}")
	@Produces(MediaType.TEXT_XML)
	public List<Client> getClientByName(@PathParam("last") String last) {
		return ClientDao.getInstance().getByName(last);
	}
	
	/**
	 * Get client from first and last name
	 * @param last
	 * @param first
	 * @return
	 */
	@GET
	@Path("{last}/{first}")
	@Produces(MediaType.TEXT_XML)
	public List<Client> getClientByFirstAndLastName(
							@PathParam("last") String last,
							@PathParam("first") String first) {
		return ClientDao.getInstance().getByFullName(last, first);
	}
	
	/**
	 * Get client from id
	 * @param id
	 * @return
	 */
	@GET
	@Path("id/{clientid}") 
	@Produces(MediaType.TEXT_XML)
	public ClientResources getClientById(@PathParam("clientid") int id) {
		return new ClientResources(uriInfo, request, id); 
	}

}