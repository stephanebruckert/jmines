package fr.ensicaen.si.resources;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import fr.ensicaen.si.dao.operation.OperationDao;
import fr.ensicaen.si.model.Operation;

@Path("/operations")
public class OperationsResources {

	@Context 
	UriInfo uriInfo; 
	@Context 
	Request request;
	
	/**
	 * Get operation from last name
	 * @param last
	 * @return
	 */
	@GET
	@Path("last/{last}")
	@Produces(MediaType.TEXT_XML)
	public List<Operation> getOperationByName(@PathParam("last") String last) {
		return OperationDao.getInstance()
				.getByName(last).getOperations();
	}
	
	/**
	 * Get operation from first and last name
	 * @param last
	 * @param first
	 * @return
	 */
	@GET
	@Path("{last}/{first}")
	@Produces(MediaType.TEXT_XML)
	public List<Operation> getOperationByFirstAndLastName(
							@PathParam("last") String last,
							@PathParam("first") String first) {
		return OperationDao.getInstance()
				.getByFullName(last, first).getOperations();
	}
	
	/**
	 * Get operation from id
	 * @param id
	 * @return
	 */
	@GET
	@Path("id/{clientid}") 
	@Produces(MediaType.TEXT_XML)
	public List<Operation> getOperationById(@PathParam("clientid") int id) {
		return OperationDao.getInstance().getById(id);
	}

}