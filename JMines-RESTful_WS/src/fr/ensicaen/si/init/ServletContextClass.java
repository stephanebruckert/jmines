package fr.ensicaen.si.init;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import fr.ensicaen.si.dao.client.ClientDao;
import fr.ensicaen.si.dao.client.DbClientDao;
import fr.ensicaen.si.dao.operation.DbOperationDao;
import fr.ensicaen.si.dao.operation.OperationDao;

public class ServletContextClass implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		System.out.println("Init DAO");
		ClientDao cd = ClientDao.getInstance();
		cd.setDelegate(new DbClientDao());
		OperationDao od = OperationDao.getInstance();
		od.setDelegate(new DbOperationDao());
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// Fermeture de JDBC
	}
}
